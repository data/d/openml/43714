# OpenML dataset: 1000-Cameras-Dataset

https://www.openml.org/d/43714

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Some camera enthusiast went and described 1,000 cameras based on 13 properties! 
Content
Row one describes the datatype for each column and can probably be removed.
The 13 properties of each camera:

Model
Release date
Max resolution
Low resolution
Effective pixels
Zoom wide (W)
Zoom tele (T)
Normal focus range
Macro focus range
Storage included
Weight (inc. batteries)
Dimensions
Price

Acknowledgements
These datasets have been gathered and cleaned up by Petra Isenberg, Pierre Dragicevic and Yvonne Jansen.  The original source can be found here.
This dataset has been converted to CSV.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43714) of an [OpenML dataset](https://www.openml.org/d/43714). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43714/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43714/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43714/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

